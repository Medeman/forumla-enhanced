/* globals chrome */

let injectedSettings = {
  headerSticky: false,
  headerCompact: false,
  relativeTimestamps: false,
  confirmBeforeUnsubscribe: false,
  attempt503Fix: false,
  maxWidth: 0
}

const normalizeSettings = settings => {
  for (let settingKey in injectedSettings) {
    injectedSettings[settingKey] = settings[settingKey] ||
      injectedSettings[settingKey]
  }
}

const saveSetting = (key, value) => {
  if (injectedSettings.hasOwnProperty(key)) {
    injectedSettings[key] = value
  }

  chrome.storage.sync.set({ injectedSettings })
}

chrome.storage.sync.get(['injectedSettings'], result => {
  if (result && result.injectedSettings) {
    normalizeSettings(result.injectedSettings)
  }

  ready()
})

const settingElements = document.querySelectorAll('.setting')

const imageURLs = () => new Promise((resolve, reject) => {
  const HOSTS = [
    {
      regexp: /^https?:\/\/giphy\.com\/gifs\/[^/]*-([^/]+)/,
      imageURL: match => (
        `https://i.giphy.com/media/${match[1]}/giphy.gif`
      )
    }/* ,
    {
      regexp: /^https?:\/\/imgur\.com\/(?:gallery|a)\/(\w+)/,
      imageURL: match => (

      )
    } */
  ]

  chrome.tabs.query({}, tabs => {
    const images = []

    for (let tab of tabs) {
      console.log(tab.url)
      for (let host of HOSTS) {
        if (host.regexp.test(tab.url)) {
          const match = tab.url.match(host.regexp)
          images.push(host.imageURL(match))
        }
      }
    }

    resolve(images)
  })
})

const ready = () => {
  for (let element of settingElements) {
    if (element.type === 'checkbox') {
      element.checked = injectedSettings[element.dataset.setting]
    } else {
      element.value = injectedSettings[element.dataset.setting]
    }

    element.addEventListener('change', () => {
      console.log('setting setting LUL')

      if (element.type === 'checkbox') {
        saveSetting(element.dataset.setting, element.checked)
      } else if (element.type === 'number') {
        saveSetting(element.dataset.setting, +element.value)
      } else {
        saveSetting(element.dataset.setting, element.value)
      }
    })
  }

  document.querySelector('#version').innerText =
    chrome.runtime.getManifest().version

  // imageURLs().then(images => {
  //   const grid = document.querySelector('#image-helper-grid')

  //   for (let image of images) {
  //     const container = document.createElement('div')
  //     container.classList.add('image-helper-container')

  //     container.addEventListener('click', () => {
  //       navigator.clipboard.writeText(`[IMG]${image}[/IMG]`)
  //     })

  //     const img = document.createElement('img')
  //     img.src = image

  //     container.appendChild(img)
  //     grid.appendChild(container)
  //   }

  //   console.log(images)
  // })
}
