/* globals chrome */

chrome.storage.sync.get(['injectedSettings'], result => {
  const settingsJSON = JSON.stringify(result.injectedSettings)

  const settings = document.createElement('script')

  settings.innerText = 'window.feSettings = ' + settingsJSON

  document.head.appendChild(settings)

  const script = document.createElement('script')

  script.src = chrome.extension.getURL('forumla-enhanced.js')

  document.head.appendChild(script)

  const link = document.createElement('link')

  link.rel = 'stylesheet'
  link.href = chrome.extension.getURL('forumla-enhanced.css')

  document.head.appendChild(link)
})
