/* globals confirm */

(function () {
  const data = {
    userSettings: {},
    styleElement: document.createElement('style')
  }

  const settings = {
    relativeTimestamps: {
      default: false,
      execute (setting) {
        if (!setting) {
          return
        }

        const TODAY_REGEXP = /Heute,\s(\d{2}):(\d{2})/
        const YESTERDAY_REGEXP = /Gestern,\s(\d{2}):(\d{2})/
        const DATE_REGEXP = /(\d{2})\.(\d{2})\.(\d{4}),\s(\d{2}):(\d{2})/
        const NOW = Date.now()
        const DURATION_YEAR = 31536e6
        const DURATION_MONTH = 2592e6
        const DURATION_WEEK = 6048e5
        const DURATION_DAY = 864e5
        const DURATION_HOUR = 36e5
        const DURATION_MINUTE = 6e4

        const elements = document.querySelectorAll('.date')

        for (let element of elements) {
          const value = element.innerText
          element.title = value
          let ts = new Date()

          ts.setSeconds(0)
          ts.setMilliseconds(0)

          if (TODAY_REGEXP.test(value)) {
            const match = value.match(TODAY_REGEXP)

            ts.setHours(+match[1])
            ts.setMinutes(+match[2])
          } else if (YESTERDAY_REGEXP.test(value)) {
            const match = value.match(YESTERDAY_REGEXP)

            ts.setDate(ts.getDate() - 1)
            ts.setHours(+match[1])
            ts.setMinutes(+match[2])
          } else if (DATE_REGEXP.test(value)) {
            const match = value.match(DATE_REGEXP)

            ts.setDate(+match[1])
            ts.setMonth(+match[2] - 1)
            ts.setYear(+match[3])
            ts.setHours(+match[4])
            ts.setMinutes(+match[5])
          } else {
            console.log('wtf?!', value)
          }

          const difference = NOW - ts

          // console.log(value, difference)

          if (difference >= DURATION_YEAR) {
            const years = Math.round(difference / DURATION_YEAR)

            if (years > 1) {
              element.innerText = `Vor ${years} Jahren`
            } else {
              element.innerText = 'Vor einem Jahr'
            }
          } else if (difference >= DURATION_MONTH) {
            const months = Math.round(difference / DURATION_MONTH)

            if (months > 1) {
              element.innerText = `Vor ${months} Monaten`
            } else {
              element.innerText = 'Vor einem Monat'
            }
          } else if (difference >= DURATION_WEEK) {
            const weeks = Math.round(difference / DURATION_WEEK)

            if (weeks > 1) {
              element.innerText = `Vor ${weeks} Wochen`
            } else {
              element.innerText = 'Vor einer Woche'
            }
          } else if (difference >= DURATION_DAY) {
            const days = Math.round(difference / DURATION_DAY)

            if (days > 1) {
              element.innerText = `Vor ${days} Tagen`
            } else {
              element.innerText = 'Vor einem Tag'
            }
          } else if (difference >= DURATION_HOUR) {
            const hours = Math.round(difference / DURATION_HOUR)

            if (hours > 1) {
              element.innerText = `Vor ${hours} Stunden`
            } else {
              element.innerText = 'Vor einer Stunde'
            }
          } else if (difference >= DURATION_MINUTE) {
            const minutes = Math.round(difference / DURATION_MINUTE)

            if (minutes > 1) {
              element.innerText = `Vor ${minutes} Minuten`
            } else {
              element.innerText = 'Vor einer Minute'
            }
          } else {
            element.innerText = 'Gerade eben'
          }
        }
      }
    },
    headerSticky: {
      default: false,
      execute (setting) {
        if (!setting) {
          return
        }

        const element = document.querySelector('.above_body')

        element.classList.add('fe-header-sticky')
      }
    },
    headerCompact: {
      default: false,
      execute (setting) {
        if (!setting) {
          return
        }

        const element = document.querySelector('.above_body')

        element.classList.add('fe-header-compact')
      }
    },
    confirmBeforeUnsubscribe: {
      default: false,
      execute (setting) {
        if (!setting) {
          return
        }

        const unsubscribeLinks = document
          .querySelectorAll('#new_subscribed_threads > ol > li .author a')

        for (let link of unsubscribeLinks) {
          if (link.innerText === 'Abonnement löschen') {
            link.addEventListener('click', e => {
              const result = confirm('Soll das Abonnement zu diesem Thema ' +
                'wirklich gelöscht werden?')

              if (!result) {
                e.preventDefault()
              }
            })
          }
        }
      }
    },
    maxWidth: {
      default: 0,
      execute (setting) {
        if (setting < 1) {
          return
        }

        const element = document.querySelector('#content_wrapper')

        element.classList.add('fe-container-center')

        element.style.maxWidth = setting + 'px'
      }
    },
    attempt503Fix: {
      default: false,
      execute (setting) {
        if (!setting) {
          return
        }

        // https://www.forumla.de/attachments/sonstige-konsolen-allgemein/181335d1510186913t-eure-schoensten-screenshots-2017111115239.jpg

        const imgTags = [].slice.call(document.querySelectorAll('img'))
          .filter(tag => tag.src.startsWith('https://www.forumla.de/attachments/'))

        for (const tag of imgTags) {
          tag.dataset.fixedSrc = `${tag.src}?_=${Math.random()}`
          tag.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg=='
        }

        let index = 0

        const next = () => {
          const tag = imgTags[index]
          ++index

          if (!tag) {
            return
          }

          tag.src = tag.dataset.fixedSrc
          tag.addEventListener('load', () => {
            next()
          })
        }

        next()
      }
    }
  }

  // const loadSettings = () => new Promise(resolve => {
  //   chrome.runtime.sendMessage({
  //     method: 'getInjectedSettings'
  //   }, response => {
  //     console.log(response.data)
  //   })
  // })

  const init = function () {
    data.userSettings = {}

    for (let i in settings) {
      data.userSettings[i] = settings[i].default
    }

    // let savedSettings = localStorage.getItem('forumlaEnhanced.settings')
    let savedSettings = window.feSettings

    console.log('amazing', savedSettings)

    if (savedSettings) {
      try {
        // const parsed = JSON.parse(savedSettings)
        Object.assign(data.userSettings, savedSettings)
      } catch (ex) {
        // @TODO
      }
    }

    console.log('fe', data.userSettings)

    for (let i in data.userSettings) {
      const setting = data.userSettings[i]

      settings[i].execute(setting)
    }

    document.head.appendChild(data.styleElement)
  }

  init()
})()
