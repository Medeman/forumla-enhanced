# Changelog

## WIP

- Neue Funktion: Bild-Tool (giphy.com) (WIP)

## 0.3.0 (2019-01-15)

- Neue Funktion: 503-Fehler beim Laden von Vorschaubildern können umgangen
  werden, indem die Bilder nacheinander geladen werden
- Anpassung: Info aus dem Footer entfernt
- Anpassung: Obsoleten/ungenutzten Code entfernt

## 0.2.0 (2018-08-19)

- Neuer Browser: Firefox
- Changelog verlinkt
- Icons PNG-8 -> PNG-24
- innerText statt innerHTML wo möglich

## 0.1.0 (2018-07-21)

- Neue Funktion: Header anheften
- Neue Funktion: Kompakter Header
- Neue Funktion: Relative Zeitstempel
- Neue Funktion: Vor "Abonnement löschen" nachfragen
- Neue Funktion: Maximale Breite
